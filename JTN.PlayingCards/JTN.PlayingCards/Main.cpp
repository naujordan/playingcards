#include <conio.h>
#include <iostream>

enum Suit 
{
	SPADES,
	DIAMONDS,
	CLUBS,
	HEARTS

};

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card
{
	Suit suit;
	Rank rank;
};

int main()
{
	_getch();
	return 0;
}